class Clinician < ActiveRecord::Base
# Validations 
# note the email feild already does some format checking 
	validates :family_name,  :presence => true
	validates :given_name,  :presence => true
	validates :phone, :presence => true,
                    :length => { :is => 8 },
                    :numericality => { :only_integer => true }
        validates :email, :uniqueness => true, :presence => true           
	
# Associations 
# Create a join table in visits with a foreign key to clinics 
# and clinicians 
	has_many :visits
	has_many :clinics , :through => :visits	
	
end
