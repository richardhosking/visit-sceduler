class Visit < ActiveRecord::Base
# Associations 
# Create a join table in visits with a foreign key to clinics 
# and clinicians 
	belongs_to :clinic
	belongs_to :clinician
	
# Use event_calendar plugin to display visits spanning multiple days 
	has_event_calendar

# Validations
# some custom validators
	validate :start_at_cannot_be_in_the_past,
	:end_at_cannot_be_before_start_date
 
  def start_at_cannot_be_in_the_past
    if !start_at.blank? and start_at < Date.today
      errors.add(:start_at, "can't be in the past")
    end
  end
 
  def end_at_cannot_be_before_start_date
  	  if end_at < start_at
  	  	  errors.add(:end_at, "can't be earlier than start date")
    end
  end
end
