class CreateClinicians < ActiveRecord::Migration
  def change
    create_table :clinicians do |t|
      t.string :given_name
      t.string :family_name
      t.string :address
      t.string :phone
      t.string :fax
      t.string :email

      t.timestamps
    end
  end
end
