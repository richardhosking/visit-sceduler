class RenameColumnsVisits < ActiveRecord::Migration
change_table :visits do |t|
	t.rename :start_date, :start_at
	t.rename :end_date, :end_at
end
end
