class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.date :start_date
      t.date :end_date
      t.timestamps
    end

  end
end
