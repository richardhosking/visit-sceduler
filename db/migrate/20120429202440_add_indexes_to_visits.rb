class AddIndexesToVisits < ActiveRecord::Migration
	# Add indexes to clinic and clinician for visits 	
	change_table :visits do |t| 
		t.references :clinic
		t.references :clinician
	end
	
	add_index :visits, :clinic_id
	add_index :visits, :clinician_id
end
