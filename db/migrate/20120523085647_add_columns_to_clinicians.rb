class AddColumnsToClinicians < ActiveRecord::Migration
	change_table :clinicians do |t|
		t.string :specialty
		t.string :display_colour
	end
end
